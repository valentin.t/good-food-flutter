# Projet good_food_flutter

Port du projet [good-food-mobile](https://framagit.org/msi-dev/good-food-mobile) en Flutter 3.0.5

## Différences fonctionnelles avec good_food_mobile

- Possibilité de supprimer des élements du panier avec un _swipe_ sur les items
- Affichage de l'historique moins chaotique
- Pas besoin de se reconnecter à chaque lancement de l'application
- Ajout d'un badge pour voir le nombre d'éléments dans le panier depuis n'importe où
- _Snackbar_ affichée lorsqu'il n'y a plus de connexion
- Système de notation/commentaires sur les plats
- Ajout d'un plat dans le panier depuis la liste des plats sur le _LongPress_ des items
- Mode nuit pouvant être activé/désactivé dans les paramètres
- **Push notification** envoyé à l'utilisateur lorsqu'une commande est mise à jour sur [good_food_web](https://framagit.org/msi-dev/good-food-web) (_Android_ uniquement)

## Installation

1. `flutter pub get` pour installer les dépendances du projet
2. Mettre en place le projet [good_food_web](https://framagit.org/msi-dev/good-food-web) pour utiliser l'api
3. Modifier l'url de l'api dans `environment/environment.dart` avant de lancer l'application

Cet application a été testé sur Android, Windows et Web (les images ne fonctionnent pas en web).

## Améliorations futures possible

À voir quoi ajouter

# Dépendances externes

- [shared_preferences](https://pub.dev/packages/shared_preferences) : Stockage en local : utilisé pour les infos utilisateur et le panier
- [http](https://pub.dev/packages/http) : Envoi de requêtes http pour communiquer avec l'api
- [email_validator](https://pub.dev/packages/email_validator) : Validation des adresses email dans login, register et settings
- [phone_form_field](https://pub.dev/packages/phone_form_field) : Input avec validation du numéro de téléphone dans settings
- [intl](https://pub.dev/packages/intl) : Paquet pour l'internationalisation : utilisé dans l'appli pour formatter les dates de commandes et de commentaires.
- [collection](https://pub.dev/packages/collection) : Manipulations dans les listes (groupBy, sum, average, ...)
- [badges](https://pub.dev/packages/badges) : Ajout de badge pour voir le nombre de plat dans le panier depuis la barre de navigation.
- [connectivity_plus](https://pub.dev/packages/connectivity_plus) : Permet d'écouter l'état de la connexion Internet
- [hive](https://pub.dev/packages/hive) : Stockage en local ayant l'air plus complet que _shared_preferences_ : utilisé uniquement pour enregistré le thème de l'application.
- Gestion des **push notifications** :
  - [firebase_core](https://pub.dev/packages/firebase_core)
  - [firebase_messaging](https://pub.dev/packages/firebase_messaging)
  - De plus, il faut installer en global **firebase-tools** (avec npm ou manuellement) et activer **flutterfire** avec dart (`dart pub global activate flutterfire_cli`), puis executer `flutterfire configure`

# Documentation/Articles/Outils utilisés
- Passage du JSON en classe Dart avec sérialisation/désérialisation : https://app.quicktype.io/
- Article sur une [architecture](https://medium.com/flutter-community/flutter-code-organization-revised-b09ad5cef7f6) dont je me suis partiellement inspiré.
- Article pour [mieux gérer les performances](https://daniyal-dolare.medium.com/how-to-use-future-builder-effectively-in-flutter-9c0550d2affd) de _FutureBuilder_
- Doc sur le [_swipe_ des éléments](https://docs.flutter.dev/cookbook/gestures/dismissible) d'une liste
- Doc sur les différentes manières de [sérialiser/désérialiser](https://docs.flutter.dev/development/data-and-backend/json) le JSON
- Doc sur la [gestion des onglets](https://docs.flutter.dev/cookbook/design/tabs)
- Doc sur la [gestion des routes](https://docs.flutter.dev/cookbook/navigation/named-routes), avec aussi une [gestion des arguments](https://docs.flutter.dev/cookbook/navigation/navigate-with-arguments)
- Doc sur le [stockage en local](https://docs.flutter.dev/cookbook/persistence/key-value) de petites quantités de données, sinon utilisez une [base SQLite](https://pub.dev/packages/sqflite)
- Doc sur [l'envoi de données](https://docs.flutter.dev/cookbook/networking/send-data) en http
- [Issue GitHub](https://github.com/flutter/flutter/issues/19330) sur la redirection depuis le `initState()` d'une page
- Article sur la [communication entre _Widget_](https://www.digitalocean.com/community/tutorials/flutter-widget-communication) sous forme de _callback_
- Article sur la gestion du [mode nuit](https://itnext.io/an-easy-way-to-switch-between-dark-and-light-theme-in-flutter-fb971155eefe)
