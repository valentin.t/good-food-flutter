import 'package:flutter/material.dart';

class DialogManager {
  static void showOkDialog(BuildContext context, String title, String message) {
    Map<String, Function()> actions = {
      'OK': () => Navigator.pop(context)
    };

    showCustomDialog(context, title, message, actions);
  }

  static void showYesNoDialog(BuildContext context, String title, String message, Function() onYes, {Function()? onNo}) {
    Map<String, Function()> actions = {
      'Oui': onYes,
      'Non': () => onNo ?? Navigator.pop(context)
    };

    showCustomDialog(context, title, message, actions);
  }

  static void showCustomDialog(BuildContext context, String title, String message, Map<String, Function()> actions) {
    List<TextButton> buttons = List<TextButton>.empty(growable: true);

    actions.forEach((key, value) {
      buttons.add(
          TextButton(
            onPressed: value,
            child: Text(key),
          )
      );
    });

    AlertDialog alert = AlertDialog(
        title: Text(title), content: Text(message), actions: buttons);

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
