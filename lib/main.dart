import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import './theme/style.dart';

import './navigation/routes.dart';

import './services/notification_service.dart';

void main() async {
  await Hive.initFlutter();
  box = await Hive.openBox('theme');
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static final FirebaseMessaging _messaging = FirebaseMessaging.instance;

  @override
  void initState() {
    super.initState();

    theme.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    pushNotificationService = NotificationService(_messaging);
    pushNotificationService.initialise();

    return MaterialApp(
        title: 'Good Food',
        theme: appTheme(),
        darkTheme: ThemeData.dark(),
        themeMode: theme.currentTheme(),
        initialRoute: '/',
        //routes: routes,
        onGenerateRoute: RouteGenerator.generateRoute,
        debugShowCheckedModeBanner: false
    );
  }
}
