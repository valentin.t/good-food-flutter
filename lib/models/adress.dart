class Adress {
  late int id;
  late String street;
  late int streetNumber;
  late String city;
  late String postcode;
  late String country;
  late DateTime createdAt;
  late DateTime updatedAt;

  Adress(
      {required this.id,
      required this.street,
      required this.streetNumber,
      required this.city,
      required this.postcode,
      required this.country,
      required this.createdAt,
      required this.updatedAt});


  @override
  String toString() {
    return '$streetNumber $street, $postcode $city';
  }

  factory Adress.fromJson(dynamic json) {
    var adress = Adress(
      id: json['id'] as int,
      street: json['street'] as String,
      streetNumber: json['street_number'] as int,
      city: json['city'] as String,
      postcode: json['postcode'] as String,
      country: json['country'] as String,
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: DateTime.parse(json['updated_at'] as String),
    );

    return adress;
  }

  Map<String, dynamic> toJson() => {
    "street": street,
    "street_number": streetNumber,
    "city": city,
    "postcode": postcode,
    "country": country,
  };

  factory Adress.fromStorage(Map<String, dynamic> json) => Adress(
    id: json["id"],
    street: json["street"],
    streetNumber: json["street_number"],
    city: json["city"],
    postcode: json["postcode"],
    country: json["country"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toStorage() => {
    "id": id,
    "street": street,
    "street_number": streetNumber,
    "city": city,
    "postcode": postcode,
    "country": country,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };

}
