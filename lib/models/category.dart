import './food.dart';

class Category {
  late int id;
  late String name;
  late DateTime createdAt;
  late DateTime updatedAt;

  List<Food> foods = List.empty(growable: true);

  Category({required this.id, required this.name, required this.createdAt, required this.updatedAt});

  factory Category.fromJson(Map<String, dynamic> json) {
    var attributes = json['attributes'] as Map<String, dynamic>;

    return Category(
        id: int.parse(json['id'] as String),
        name: attributes['name'] as String,
        // J'ai oublié de les mettre sur la route principale
        createdAt: DateTime.now(),
        updatedAt: DateTime.now()
    );
  }

  factory Category.fromFoodJson(Map<String, dynamic> json) {
    return Category(
      id: json['id'] as int,
      name: json['name'] as String,
      createdAt: DateTime.now(),//DateTime.parse(json['created_at'] as String),
      updatedAt: DateTime.now()//DateTime.parse(json['updated_at'] as String),
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'created_at': createdAt.toIso8601String(),
    'updated_at': updatedAt.toIso8601String()
  };
}