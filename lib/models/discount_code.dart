import './discount_type.dart';

// Code promo dans l'API
class DiscountCode {
  late int id;
  late String codeName;
  late double amount;
  late int typePromoId;
  int? franchiseId;

  DiscountType? discountType;

  DiscountCode({required this.id,
                required this.codeName,
                required this.amount,
                required this.typePromoId,
                this.franchiseId});

  factory DiscountCode.fromJson(Map<String, dynamic> json) {
    var code =  DiscountCode(
        id: json['id'],
        codeName: json['code_name'],
        amount: double.parse(json['amount']),
        typePromoId: json['type_promo_id']
    );

    if (json.containsKey('type_promo')) {
      code.discountType = DiscountType.fromJson(json['type_promo']);
    }

    return code;
  }
}