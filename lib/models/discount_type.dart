import './discount_code.dart';

// TypePromo dans l'API
class DiscountType {
  late int id;
  late String name;

  List<DiscountCode> discountCodes = List.empty(growable: true);

  DiscountType({required this.id, required this.name});

  factory DiscountType.fromJson(Map<String, dynamic> json) {
    return  DiscountType(
        id: json['id'],
        name: json['name']
    );
  }
}