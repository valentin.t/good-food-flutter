import 'package:collection/collection.dart';

import '../environment/environment.dart';

import './review.dart';
import './category.dart';

class Food {
  late int id;
  late String name;
  late String description;
  late int numberConsumer;
  late double price;
  late int categoryId;
  late bool isActive;
  late bool isArchived;
  String? urlImage;
  late DateTime createdAt;
  late DateTime updatedAt;

  Category? category;
  List<Review> reviews = List<Review>.empty();

  String get displayPrice => '${price.toStringAsFixed(2)} €';
  double? get averageReview =>
      reviews.isNotEmpty ? reviews.map((r) => r.note).average : null;

  Food(
      {required this.id,
      required this.name,
      required this.description,
      required this.numberConsumer,
      required this.price,
      required this.categoryId,
      required this.isActive,
      required this.isArchived,
      required this.createdAt,
      required this.updatedAt,
      this.urlImage,
      this.category});

  factory Food.fromJson(Map<String, dynamic> json) {
    var categoryInfos = json['category'] as Map<String, dynamic>;

    var food = Food(
      id: json['id'] as int,
      name: json['name'] as String,
      description: json['description'] as String,
      numberConsumer: json['number_consumer'] as int,
      price: double.parse(json['price'] as String),
      categoryId: json['category_id'] as int,
      //franchiseId
      isActive: json['is_active'] as bool,
      isArchived: json['is_archived'] as bool,
      urlImage: json['url'] as String?,
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: DateTime.parse(json['updated_at'] as String),
    );

    if (food.urlImage != null) {
      food.urlImage = 'http://$URL_BACK_END${food.urlImage!}';
    }

    food.category = Category.fromFoodJson(categoryInfos);

    if (json.containsKey('reviews')) {
      var reviewsInfos = json['reviews'] as List<dynamic>;
      food.reviews = reviewsInfos.map((r) => Review.fromFoodJson(r)).toList();
    }

    return food;
  }

  factory Food.fromOrderJson(Map<String, dynamic> json) {
    return Food(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'] as String,
        numberConsumer: json['number_consumer'] as int,
        price: double.parse(json['price']),
        categoryId: json['category_id'] as int,
        //franchiseId
        isActive: json['is_active'] as bool,
        isArchived: json['is_archived'] as bool,
        createdAt:
            DateTime.now(), //DateTime.parse(json['created_at'] as String),
        updatedAt:
            DateTime.now() //DateTime.parse(json['updated_at'] as String),
        );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'number_consumer': numberConsumer,
        'price': price.toStringAsFixed(2),
        'category_id': categoryId,
        //franchiseId
        'is_active': isActive,
        'is_archived': isArchived,
        'url': urlImage,
        'created_at': createdAt.toIso8601String(),
        'updated_at': updatedAt.toIso8601String(),
        'category': category?.toJson()
      };
}
