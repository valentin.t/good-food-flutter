import './rank.dart';
import './adress.dart';

class Member {
  late int id;
  late String firstname;
  late String lastname;
  late String email;
  String? phone;
  late bool activate2FA;
  late double price;
  DateTime? lastConnection;
  late String passwordDigest;
  String? token;
  late bool userVerified;
  late int rankId;
  int? adressId;
  int? franchiseId;
  late DateTime createdAt;
  late DateTime updatedAt;

  Adress? adress;
  Rank? rank;

  Member({
    required this.id,
    required this.firstname,
    required this.lastname,
    required this.email,
    this.phone,
    required this.activate2FA,
    required this.lastConnection,
    required this.passwordDigest,
    required this.token,
    required this.userVerified,
    required this.rankId,
    this.adressId,
    this.franchiseId,
    required this.createdAt,
    required this.updatedAt});

  //Les créateurs de Dart ne permettent pas de le faire nativement
  // DeepCopy -> pour avoir un objet avec les mêmes valeurs sans avoir la même ref
  factory Member.clone(Member this_) {
    return Member(
      id : this_.id,
      firstname : this_.firstname,
      lastname : this_.lastname,
      email : this_.email,
      phone : this_.phone,
      activate2FA : this_.activate2FA,
      lastConnection : this_.lastConnection,
      passwordDigest : this_.passwordDigest,
      token : this_.token,
      userVerified : this_.userVerified,
      rankId : this_.rankId,
      adressId : this_.adressId,
      franchiseId : this_.franchiseId,
      createdAt : this_.createdAt,
      updatedAt : this_.updatedAt
    );
  }

  Map<String, dynamic> toJson(Member initialMember) {
    List<MapEntry<String, dynamic>> jsonValues = List.empty(growable: true);

    jsonValues.add(MapEntry("firstname", firstname));
    jsonValues.add(MapEntry("lastname", lastname));

    if (email != initialMember.email) {
      jsonValues.add(MapEntry("lastname", lastname));
    }

    if (phone != initialMember.phone) {
      jsonValues.add(MapEntry("phone", phone));
    }

    if(adress != null && adress != initialMember.adress) {
      jsonValues.add(MapEntry("adress", adress?.toJson()));
    }

    return Map<String, dynamic>.fromEntries(jsonValues);
  }

  factory Member.fromJson(dynamic json) {
    var memberInfos = json['member'] as Map<String, dynamic>;
    var adressInfos = json['adress'] as dynamic;

    var member = Member(
        id: memberInfos['id'] as int,
        firstname: memberInfos['firstname'] as String,
        lastname: memberInfos['lastname'] as String,
        email: memberInfos['email'] as String,
        phone: memberInfos['phone'] as String?,
        activate2FA: memberInfos['activate_2FA'] as bool,
        lastConnection: memberInfos['last_connection'] != null ? DateTime.parse(memberInfos['last_connection'] as String) : null,
        passwordDigest: memberInfos['password_digest'] as String,
        token: memberInfos['token'] as String?,
        userVerified: memberInfos['user_verified'] as bool,
        rankId: memberInfos['rank_id'] as int,
        adressId: memberInfos['adress_id'] as int?,
        //franchiseId
        createdAt: DateTime.parse(memberInfos['created_at'] as String),
        updatedAt: DateTime.parse(memberInfos['updated_at'] as String),
    );

    member.rank = Rank(id: member.rankId, name: json['rank'] as String);

    if (member.adressId != null && adressInfos != null && adressInfos.isNotEmpty) {
      member.adress = Adress.fromJson(adressInfos);
    }

    return member;
  }

  factory Member.fromUpdateJson(dynamic json) {
    var memberInfos = json['attributes'] as Map<String, dynamic>;
    var adressInfos = memberInfos['adress'] as dynamic;
    var rankInfos = memberInfos['rank'] as dynamic;

    var member = Member(
      id: int.parse(json["id"] as String),
      firstname: memberInfos['firstname'] as String,
      lastname: memberInfos['lastname'] as String,
      email: memberInfos['email'] as String,
      phone: memberInfos['phone'] as String?,
      activate2FA: false,
      lastConnection: memberInfos.containsKey('last_connection') && memberInfos['last_connection'] != null ? DateTime.parse(memberInfos['last_connection'] as String) : null,
      passwordDigest: '',
      token: null,
      userVerified: memberInfos['user_verified'] as bool,
      rankId: memberInfos['rank_id'] as int,
      adressId: adressInfos?['id'] as int?,
      //franchiseId
      updatedAt: DateTime.now(),
      createdAt: DateTime.now()
    );

    member.rank = Rank.fromStorage(rankInfos);

    if (member.adressId != null && adressInfos != null && adressInfos.isNotEmpty) {
      member.adress = Adress.fromJson(adressInfos);
    }

    return member;
  }

  factory Member.fromStorage(dynamic storage) {
    var adressInfos = storage?['adress'];

    var member = Member(
      id: storage['id'] as int,
      firstname: storage['firstname'] as String,
      lastname: storage['lastname'] as String,
      email: storage['email'] as String,
      phone: storage['phone'] as String?,
      activate2FA: storage['activate_2FA'] as bool,
      lastConnection: storage['last_connection'] != null ? DateTime.parse(storage['last_connection'] as String) : null,
      passwordDigest: storage['password_digest'] as String,
      token: storage['token'] as String?,
      userVerified: storage['user_verified'] as bool,
      rankId: storage['rank_id'] as int,
      adressId: storage['adress_id'] as int?,
      //franchiseId
      createdAt: DateTime.parse(storage['created_at'] as String),
      updatedAt: DateTime.parse(storage['updated_at'] as String),
    );

    member.rank = Rank.fromStorage(storage['rank']);

    if (member.adressId != null && adressInfos != null && adressInfos.isNotEmpty) {
      member.adress = Adress.fromStorage(adressInfos);
    }

    return member;
  }

  Map<String, dynamic> toStorage() => {
    "id": id,
    "firstname": firstname,
    "lastname": lastname,
    "email": email,
    "phone": phone,
    "activate_2FA": activate2FA,
    "last_connection": lastConnection?.toIso8601String(),
    "password_digest": passwordDigest,
    "token": token,
    "user_verified": userVerified,
    "rank_id": rankId,
    "adress_id": adressId,
    //"franchise_id": franchiseId,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "rank": rank?.toStorage(),
    "adress": adress?.toStorage(),
  };
}