import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:collection/collection.dart';

import '../theme/style.dart';

import './discount_code.dart';
import './food.dart';
import './order_status.dart';

class Order {
  late int id;
  late DateTime orderDate;
  String? name;
  late double totalAmount;
  int? codePromoId;
  int? franchiseId;
  int? memberId;
  late int orderStatusId;
  late DateTime createdAt;
  late DateTime updatedAt;

  OrderStatus? orderStatus;
  DiscountCode? codePromo;
  List<Food> foods = List.empty(growable: true);

  String get displayTotal =>
      '${foods.map((e) => e.price).sum.toStringAsFixed(2)} €';
  String get displayDiscount => '${totalAmount.toStringAsFixed(2)} €';
  String get displayDate =>
      'le ${DateFormat('dd/MM/yyyy à HH:mm:ss').format(orderDate)}';

  Color historyBackgroundColor(BuildContext context) {
    late Color color;

    switch (orderStatusId) {
      // Livraison en cours
      case 2:
        color = theme.isDarkTheme ? Colors.black26 : Colors.yellow.shade200;
        break;

      // Commande livrée
      case 3:
        color = theme.isDarkTheme ? Colors.black54 : Colors.orange.shade200;
        break;

    // En préparation (1) et autres
      default:
        color = Colors.transparent;
        break;
    }

    return color;
  }

  Order(
      {required this.id,
      required this.orderDate,
      this.name,
      required this.totalAmount,
      this.codePromoId,
      this.franchiseId,
      this.memberId,
      required this.orderStatusId,
      required this.createdAt,
      required this.updatedAt});

  factory Order.fromJson(Map<String, dynamic> json) {
    var statusInfos = json['order_status'];
    var foodsInfos = json['foods'] as List<dynamic>;

    var order = Order(
      id: json['id'] as int,
      orderDate: DateTime.parse(json['order_date'] as String),
      //name
      totalAmount: double.parse(json['total_amount']),
      codePromoId: json['code_promo_id'] as int?,
      //franchiseId
      orderStatusId: json['order_status_id'] as int,
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: DateTime.parse(json['updated_at'] as String),
    );

    order.orderStatus = OrderStatus.fromOrderJson(statusInfos);

    if (order.codePromoId != null) {
      order.codePromo = DiscountCode.fromJson(json['code_promo']);
    }

    order.foods = foodsInfos.map((f) => Food.fromOrderJson(f)).toList();

    return order;
  }

  factory Order.fromStatusJson(Map<String, dynamic> json) {
    return Order(
        id: json['id'] as int,
        orderDate:
            DateTime.now(), //DateTime.parse(json['order_date'] as String),
        //name
        totalAmount: json['total_amount'] as double,
        codePromoId: json['code_promo_id'] as int?,
        //franchiseId
        orderStatusId: json['order_status_id'] as int,
        createdAt:
            DateTime.now(), //DateTime.parse(json['created_at'] as String),
        updatedAt:
            DateTime.now() //DateTime.parse(json['updated_at'] as String),
        );
  }

  Map<String, dynamic> toJson() => {
        'total_amount': totalAmount,
        'code_promo_id': codePromoId,
        'member_id': memberId!,
        'foods': foods.map((f) => f.toJson()).toList()
      };
}
