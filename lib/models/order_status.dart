import './order.dart';

class OrderStatus {
  late int id;
  late String name;
  late String description;
  late DateTime createdAt;
  late DateTime updatedAt;

  List<Order> orders = List.empty(growable: true);

  OrderStatus({required this.id, required this.name, required this.description, required this.createdAt, required this.updatedAt});

  factory OrderStatus.fromJson(Map<String, dynamic> json) {
    var ordersInfos = json['orders'] as List<Map<String, dynamic>>;

    var orderStatus = OrderStatus(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'] as String,
        createdAt: DateTime.now(), //DateTime.parse(json['created_at'] as String),
        updatedAt: DateTime.now() //DateTime.parse(json['updated_at'] as String)
    );

    ordersInfos.map((o) => orderStatus.orders.add(Order.fromJson(o)));

    return orderStatus;
  }

  factory OrderStatus.fromOrderJson(Map<String, dynamic> json) {
    return OrderStatus(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'] as String,
        createdAt: DateTime.now(),//DateTime.parse(json['created_at'] as String),
        updatedAt: DateTime.now() //DateTime.parse(json['updated_at'] as String)
    );
  }
}