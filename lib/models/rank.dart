class Rank {
  late int id;
  late String name;
  DateTime? createdAt;
  DateTime? updatedAt;

  Rank({
    required this.id,
    required this.name,
    this.createdAt,
    this.updatedAt});

  factory Rank.fromStorage(Map<String, dynamic> storage) => Rank(
    id: storage["id"],
    name: storage["name"],
  );

  Map<String, dynamic> toStorage() => {
    "id": id,
    "name": name,
  };
}