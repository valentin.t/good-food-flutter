import 'package:intl/intl.dart';

class Review {
  int id;
  double note;
  String comment;
  int? foodId;
  int? menuId;
  DateTime createdAt;
  DateTime updatedAt;

  Review({
    required this.id,
    required this.note,
    required this.comment,
    this.foodId,
    this.menuId,
    required this.createdAt,
    required this.updatedAt});

  String get displayDate => 'le ${DateFormat('dd/MM/yyyy à HH:mm:ss').format(createdAt)}';

  factory Review.fromJson(Map<String, dynamic> json) {
    var attributes = json['attributes'];

    return Review(
      id: int.parse(json['id']),
      note: double.parse(attributes['note']),
      comment: attributes['comment'],
      foodId: attributes['food_id'] as int?,
      menuId: attributes['menu_id'] as int?,
      createdAt: DateTime.parse(attributes['created_at']),
      updatedAt: DateTime.parse(attributes['updated_at'])
    );
  }

  factory Review.fromFoodJson(Map<String, dynamic> json) {
    return Review(
        id: json['id'],
        note: double.parse(json['note']),
        comment: json['comment'],
        foodId: json['food_id'] as int?,
        menuId: json['menu_id'] as int?,
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at'])
    );
  }

  Map<String, dynamic> toJson() => {
    "note": note.toStringAsFixed(1),
    "comment": comment,
    "food_id": foodId,
  };
}