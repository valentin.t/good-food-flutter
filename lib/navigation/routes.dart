import 'package:flutter/material.dart';

import '../screens/auth/login/login.dart';
import '../screens/auth/register/register.dart';
import '../screens/home/home.dart';
import '../screens/detail_food/detail_food.dart';

import '../models/food.dart';

// Gestion des routes sans arguments
/*final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  "/": (BuildContext context) => const Login(),
  "/register": (BuildContext context) => const Register(),
  "/home": (BuildContext context) => const Home(),
  "/detail": (BuildContext context) => const DetailFood()
};*/

class RouteGenerator {

  // Si il y a besoin d'arguments pour une ou plusieurs routes, il faut écrire la fonction OnGenerateRoute du MaterialApp dans main
  static Route<dynamic>  generateRoute(RouteSettings settings, {bool isAlreadyLogged = false}) {
    final args = settings.arguments;
    Route route;

    switch (settings.name) {
      case '/':
        route = MaterialPageRoute(builder: (_) {
          return isAlreadyLogged ? const Home() : const Login();
        });
        break;

      case '/login':
        route = MaterialPageRoute(builder: (_) => const Login());
        break;

      case '/register':
        route = MaterialPageRoute(builder: (_) => const Register());
        break;

      case '/home':
        route = MaterialPageRoute(builder: (_) => const Home());
        break;

      case '/detail':
        var argsArray = args as List<dynamic>;
        var food = argsArray[0] as Food;
        var callback = argsArray[1] as ValueChanged<int>;

        route = MaterialPageRoute(builder: (_) => DetailFood(selectedFood: food, onAddItem: callback));
        break;

      default:
        route = _errorRoute();
        break;
    }

    return route;
  }

  // Route d'erreur avec page standard
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Erreur'),
        ),
        body: const Center(
          child: Text('Erreur de navigation.'),
        ),
      );
    });
  }
}