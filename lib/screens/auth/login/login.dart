import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:async';

import '../../../theme/style.dart';

import '../../../validator.dart';
import '../../../dialog_manager.dart';

import '../../../storage/auth_storage.dart';
import '../../../services/auth_service.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late StreamSubscription _subscription;
  final _formKey = GlobalKey<FormState>();
  String email = '';
  String password = '';

  @override
  void initState() {
    super.initState();

    _subscription = Connectivity().onConnectivityChanged.listen((result) {
      if (result == ConnectivityResult.none) {
        ScaffoldMessenger.of(context).clearSnackBars();

        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Aucune connexion.'),
          duration: Duration(days: 100),
          dismissDirection: DismissDirection.none,
        ));
      } else {
        ScaffoldMessenger.of(context).clearSnackBars();
      }
    });

    AuthService.isLoggedIn().then((logged) {
      if (logged) {
        Future.microtask(_navigateToHomepage);
      }
    }).catchError((e) {
      // Pour obtenir l'état de connection lors de l'initialisation
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('Aucune connexion.'),
        duration: Duration(days: 100),
        dismissDirection: DismissDirection.none,
      ));
    });
  }

  @override
  void dispose() {
    super.dispose();
    _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: const Text('Connexion'),
            centerTitle: true,
          ),
          body: Container(
            margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.alternate_email),
                      border: UnderlineInputBorder(),
                      labelText: 'Email',
                    ),
                    keyboardType: TextInputType.emailAddress,
                    maxLength: 50,
                    onChanged: (value) => email = value.trim(),
                    validator: (value) => Validator.validateEmail(value),
                  ),
                  const SizedBox(height: 5),
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      border: UnderlineInputBorder(),
                      labelText: 'Mot de passe',
                    ),
                    obscureText: true,
                    maxLength: 64,
                    onChanged: (value) => password = value,
                    validator: (value) => Validator.validate(value),
                  ),
                  const SizedBox(height: 5),
                  ElevatedButton(
                    style: elevatedButtonStyle(),
                    onPressed: _login,
                    child: const Text('SE CONNECTER'),
                  ),
                  const SizedBox(height: 40),
                  const Center(
                    child: Text('Pas de compte ?'),
                  ),
                  const SizedBox(height: 5),
                  ElevatedButton(
                    style: elevatedButtonStyle(),
                    onPressed: _register,
                    child: const Text('S\'INSCRIRE'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _login() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    AuthService.connectAccount(email, password).then((result) {
      if (!result.key) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: const Text('Non connecté'),
              action: SnackBarAction(
                  label: 'Voir',
                  onPressed: () => DialogManager.showOkDialog(
                      context, 'Erreur de connexion', result.value))),
        );
        return;
      }

      _navigateToHomepage();
    });
  }

  void _register() {
    Navigator.pushNamed(context, '/register');
  }

  void _navigateToHomepage() {
    AuthStorage.getCurrentUser().then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Bienvenue, ${value.firstname} ${value.lastname} !'),
        ),
      );

      pushNotificationService.subscribeTopic('member-${value.id}').whenComplete(() => Navigator.pushNamed(context, '/home'));
    });
  }
}
