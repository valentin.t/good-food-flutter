import 'package:flutter/material.dart';

import '../../../theme/style.dart';
import  '../../../validator.dart';
import '../../../dialog_manager.dart';

import '../../../services/auth_service.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  String firstname = '';
  String lastname = '';
  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Inscription'),
          centerTitle: true,
        ),
        body: Container(
          margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(
                    prefixIcon: Icon(Icons.fingerprint_outlined),
                    border: UnderlineInputBorder(),
                    labelText: 'Prénom',
                  ),
                  keyboardType: TextInputType.name,
                  maxLength: 50,
                  onChanged: (value) => firstname = value.trim(),
                  validator:(value) => Validator.validate(value),
                ),
                const SizedBox(height: 5),
                TextFormField(
                  decoration: const InputDecoration(
                    prefixIcon: Icon(Icons.person_outline),
                    border: UnderlineInputBorder(),
                    labelText: 'Nom',
                  ),
                  keyboardType: TextInputType.name,
                  maxLength: 50,
                  onChanged: (value) => lastname = value.trim(),
                  validator: (value) => Validator.validate(value),
                ),
                const SizedBox(height: 5),
                TextFormField(
                  decoration: const InputDecoration(
                    prefixIcon: Icon(Icons.alternate_email),
                    border: UnderlineInputBorder(),
                    labelText: 'Email',
                  ),
                  keyboardType: TextInputType.emailAddress,
                  maxLength: 50,
                  onChanged: (value) => email = value.trim(),
                  validator: (value) => Validator.validateEmail(value),
                ),
                const SizedBox(height: 5),
                TextFormField(
                  decoration: const InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    border: UnderlineInputBorder(),
                    labelText: 'Mot de passe',
                  ),
                  obscureText: true,
                  maxLength: 64,
                  onChanged: (value) => password = value,
                  validator: (value) => Validator.validate(value),
                ),
                const SizedBox(height: 5),
                ElevatedButton(
                  style: elevatedButtonStyle(),
                  onPressed: _register,
                  child: const Text('S\'INSCRIRE'),
                ),
                const SizedBox(height: 40),
                const Center(
                  child: Text('Déjà un compte ?'),
                ),
                const SizedBox(height: 5),
                ElevatedButton(
                  style: elevatedButtonStyle(),
                  onPressed: _login,
                  child: const Text('SE CONNECTER'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _register() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    AuthService.registerAccount(firstname, lastname, email, password)
        .then((result) {
      if (!result.key) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
              content: const Text('Erreur lors de la création du compte'),
              action: SnackBarAction(
                  label: 'Voir l\'erreur',
                  onPressed: () => DialogManager.showOkDialog(context, 'Erreur d\'inscription', result.value)
              )
          ),
        );
        return;
      }

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Compte crée'),
        ),
      );
      Navigator.pop(context);
    });
  }

  void _login() {
    Navigator.pop(context);
  }
}
