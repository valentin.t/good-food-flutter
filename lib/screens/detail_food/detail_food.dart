import 'package:flutter/material.dart';

import '../../theme/style.dart';
import '../../theme/theme_manager.dart';
import '../../validator.dart';

import '../../storage/cart_storage.dart';

import '../../services/food_service.dart';

import '../../models/food.dart';
import '../../models/review.dart';

class DetailFood extends StatefulWidget {
  final ValueChanged<int> onAddItem;
  final Food? selectedFood;
  const DetailFood({this.selectedFood, required this.onAddItem, Key? key})
      : super(key: key);

  @override
  State<DetailFood> createState() => _DetailFoodState();
}

class _DetailFoodState extends State<DetailFood> {
  final _formKey = GlobalKey<FormState>();
  String _note = '';
  String _comment = '';

  @override
  Widget build(BuildContext context) {
    if (super.widget.selectedFood == null) {
      return const Center(child: Text('Erreur, aucun plat trouvé'));
    }

    return FutureBuilder<void>(
      future: _fetchReviews(),
      builder: ((context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else {
          return _getScreen();
        }
      }),
    );
  }

  void _addToCart(Food food) {
    CartStorage.createOrAddToCart(food).then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Plat ajouté'),
          duration: Duration(seconds: 2),
        ),
      );
    }).whenComplete(() {
      CartStorage.getCart()
          .then((value) => super.widget.onAddItem(value.length));
    });
  }

  Scaffold _getScreen() {
    final food = super.widget.selectedFood!;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(food.name),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.only(left: 15, right: 15, top: 30),
          child: Column(
            children: <Widget>[
              Visibility(
                  maintainSize: false,
                  visible: food.urlImage != null,
                  child: Image.network(food.urlImage.toString())),
              const SizedBox(height: 20),
              Row(children: <Widget>[
                const SizedBox(width: 10),
                Text(food.displayPrice),
                const Spacer(),
                Text(
                    '${food.averageReview?.toStringAsFixed(1) ?? 'Aucune note'} '),
                Visibility(
                  visible: food.averageReview != null,
                  child: const Icon(Icons.star),
                ),
                const SizedBox(width: 10),
              ]),
              const SizedBox(height: 20),
              Text(food.description),
              const SizedBox(height: 30),
              ElevatedButton(
                style: elevatedButtonStyle(),
                onPressed: () => _addToCart(food),
                child: const Text('AJOUTER AU PANIER'),
              ),
              const SizedBox(height: 20),
              _getComments(),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _promptFormReview,
        backgroundColor: Colors.blue,
        child: const Icon(Icons.add, color: Colors.white),
      ),
    );
  }

  Widget _getComments() {
    if (super.widget.selectedFood!.reviews.isEmpty) {
      return const Center(child: Text('Aucun commentaire'));
    }

    final reviews = super.widget.selectedFood!.reviews;

    return ListView.separated(
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: reviews.length,
      separatorBuilder: (context, index) => const Divider(),
      itemBuilder: (context, index) {
        final currentReview = reviews[index];

        return ExpansionTile(
            expandedAlignment: Alignment.centerLeft,
            iconColor: Colors.black,
            title: Text('Anonyme ${currentReview.displayDate}'),
            controlAffinity: ListTileControlAffinity.leading,
            trailing: Row(mainAxisSize: MainAxisSize.min, children: [
              Text('${currentReview.note}'),
              Icon(Icons.star, color: ThemeManager().isDarkTheme ? Colors.white : Colors.black)
            ]),
            children: [Text(currentReview.comment)]);
      },
    );
  }

  Future<void> _fetchReviews() async {
    if (super.widget.selectedFood != null) {
      var listReviews =
          await FoodService.getReviewsOfFood(super.widget.selectedFood!);

      super.widget.selectedFood!.reviews = listReviews;
    }
  }

  void _promptFormReview() {
    AlertDialog alert = AlertDialog(
        scrollable: true,
        title: const Text('Nouvelle note'),
        content: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Note',
                ),
                initialValue: _note,
                keyboardType: TextInputType.number,
                maxLength: 1,
                onChanged: (value) => _note = value.trim(),
                validator: (value) =>
                    Validator.validateInt(value, minValue: 0, maxValue: 5),
              ),
              TextFormField(
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Commentaire',
                ),
                initialValue: _comment,
                maxLines: 4,
                keyboardType: TextInputType.multiline,
                onChanged: (value) => _comment = value.trim(),
                validator: (value) => Validator.validate(value),
              ),
            ],
          ),
        ),
        actions: [
          TextButton(onPressed: _saveReview, child: const Text('Valider')),
          TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Annuler'))
        ]);

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _saveReview() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    var note = double.parse(_note);
    var review = Review(
        id: -1,
        note: note,
        comment: _comment,
        foodId: super.widget.selectedFood?.id ?? 0,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now());

    FoodService.createReview(review).then((value) {
      String msg =
          value ? "Note enregistrée." : "Erreur lors de la création de la note";

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(msg)));

      if (value) {
        _note = '';
        _comment = '';
        setState(() {
          _fetchReviews();
        });
      }
    }).whenComplete(() => Navigator.pop(context));
  }
}
