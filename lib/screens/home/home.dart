import 'package:flutter/material.dart';
import 'package:badges/badges.dart';

import '../../../theme/style.dart';

import '../../../storage/cart_storage.dart';

import './pages/list_food.dart';
import './pages/cart.dart';
import './pages/history.dart';
import './pages/settings.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  late TabController _tabController;
  int _selectedTab = 0;
  int _cartCounter = 0;

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    CartStorage.getCart().then((value) => _cartCounter = value.length);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var listTabs = _getTabItems();

    // WillPopScore permet de gérer l'évent de retour en arrière (onWillPop)
    // Dans ce cas on empêche l'utilisateur de revenir en arrière
    return WillPopScope(
        onWillPop: () async => false,
        child: DefaultTabController(
            length: listTabs.length,
            child: Scaffold(
              bottomNavigationBar: _getTabBar(listTabs),
              body: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _tabController,
                children: <Widget>[
                  ListFood(onAddItem: _refreshCounter),
                  Cart(onDeleteItem: _refreshCounter),
                  const History(),
                  Settings(onChangeTheme: _refreshNavBar)
                ],
              ),
            )));
  }

  BottomNavigationBar _getTabBar(List<BottomNavigationBarItem> listItems) {
    return BottomNavigationBar(
      currentIndex: _selectedTab,
      type: BottomNavigationBarType.fixed,
      backgroundColor: theme.isDarkTheme ? Colors.transparent : Colors.white,
      unselectedItemColor: Colors.grey,
      selectedItemColor: theme.isDarkTheme ? Colors.white : Colors.black,
      items: listItems,
      onTap: (value) {
        _tabController.animateTo(value);
        setState(() => _selectedTab = value);
      },
    );
  }

  List<BottomNavigationBarItem> _getTabItems() {
    return <BottomNavigationBarItem>[
      const BottomNavigationBarItem(
        icon: Icon(Icons.food_bank_outlined),
        label: 'Plats',
      ),
      BottomNavigationBarItem(
        icon: Badge(
          showBadge: _cartCounter != 0,
          badgeContent: Text(_cartCounter.toString(),
              style: const TextStyle(color: Colors.white)),
          animationType: BadgeAnimationType.scale,
          child: const Icon(Icons.shopping_cart),
        ),
        label: 'Panier',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.history_outlined),
        label: 'Historique',
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.manage_accounts_outlined),
        label: 'Mon compte',
      ),
    ];
  }

  void _refreshCounter(int newValue) {
    setState(() {
      _cartCounter = newValue;
    });
  }

  void _refreshNavBar() {
    setState(() => 0);
  }
}
