import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

import '../../../theme/style.dart';

import '../../../dialog_manager.dart';

import '../../../storage/cart_storage.dart';
import '../../../storage/auth_storage.dart';

import '../../../services/order_service.dart';

import '../../../models/order.dart';
import '../../../models/food.dart';

class Cart extends StatefulWidget {
  final ValueChanged<int> onDeleteItem;
  const Cart({Key? key, required this.onDeleteItem}) : super(key: key);

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  final _formKey = GlobalKey<FormState>();
  late List<Food> _foods;
  Future<List<Food>> futureValue = CartStorage.getCart();
  String _codePromo = '';
  double? _totalFoods;
  double? _totalOrder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Food>>(
      future: futureValue,
      builder: ((context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting ||
            !snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else {
          _foods = snapshot.data?.toList() ?? List<Food>.empty();
          _totalFoods =
              _foods.isNotEmpty ? _foods.map((f) => f.price).sum : null;
          return _getScreen();
        }
      }),
    );
  }

  Scaffold _getScreen() {
    final list = _getCart();

    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Votre panier'),
          centerTitle: true,
        ),
        body: Form(
            key: _formKey,
            child: Container(
              margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.discount_outlined),
                      border: UnderlineInputBorder(),
                      labelText: 'Code promo (optionnel)',
                    ),
                    keyboardType: TextInputType.name,
                    maxLength: 20,
                    onChanged: (value) => _codePromo = value.trim(),
                  ),
                  Visibility(
                      visible: _totalFoods != null,
                      child: Column(
                        children: <Widget>[
                          Center(
                              child: Text(
                                  'Total : ${_totalFoods?.toStringAsFixed(2)} €',
                                  style: totalFoodTextStyle())),
                          const SizedBox(height: 10),
                        ],
                      )),
                  ElevatedButton(
                    style: elevatedButtonStyle(),
                    onPressed: _makeOrder,
                    child: const Text('VALIDER LA COMMANDE'),
                  ),
                  Expanded(child: list)
                ],
              ),
            )));
  }

  Widget _getCart() {
    if (_foods.isEmpty) {
      return const Center(child: Text('Panier vide !'));
    }

    return ListView.separated(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: _foods.length,
      separatorBuilder: (context, index) => const Divider(),
      itemBuilder: (context, index) {
        final currentFood = _foods[index];

        return Dismissible(
          // Each Dismissible must contain a Key. Keys allow Flutter to
          // uniquely identify widgets.
          key: GlobalKey(),
          // Provide a function that tells the app
          // what to do after an item has been swiped away.
          onDismissed: (direction) async {
            String deletedName = 'Plat';

            // Remove the item from the data source.
            await CartStorage.deleteCartItem(index);
            setState(() {
              deletedName = _foods.removeAt(index).name;
              _totalFoods =
              _foods.isNotEmpty ? _foods.map((f) => f.price).sum : null;
              super.widget.onDeleteItem(_foods.length);
              futureValue = CartStorage.getCart();
            });

            // Then show a snackbar.
            if (mounted) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text('$deletedName retiré du panier'),
                duration: const Duration(seconds: 2),
              ));
            }
          },
          // Show a red background as the item is swiped away.
          background: Container(
              color: Colors.red,
              child: Row(children: const <Widget>[
                SizedBox(width: 10),
                Icon(Icons.delete_outline, color: Colors.white),
                Spacer(),
                Icon(Icons.delete_outline, color: Colors.white),
                SizedBox(width: 10)
              ])),
          child: ListTile(
            title: Text(currentFood.name),
            subtitle: Text(currentFood.displayPrice),
          ),
        );
      },
    );
  }

  void _makeOrder() {
    if (_totalFoods == null) {
      return;
    }

    if (_codePromo.isNotEmpty) {
      int idPromo = 0;
      _totalOrder = _totalFoods;

      OrderService.getDiscount(_codePromo, _totalOrder!).then((pair) {
        if (pair.key != null) {
          _totalOrder = _totalOrder! - pair.key!;
          idPromo = pair.value!;
          _saveOrder(idPromo, _totalOrder!);
        } else {
          DialogManager.showYesNoDialog(context, 'Code Promo invalide',
              'Ce code promo est inexistant ou expiré. Valider quand même la commande ?',
              () {
            _saveOrder(idPromo, _totalOrder!);
            Navigator.pop(context);
          });
        }
      });
    } else {
      _saveOrder(0, _totalFoods!);
    }
  }

  void _saveOrder(int promo, double total) {
    Order order = Order(
        id: 0,
        orderDate: DateTime.now(),
        totalAmount: double.parse(total.toStringAsFixed(2)),
        orderStatusId: 1,
        createdAt: DateTime.now(),
        updatedAt: DateTime.now());

    order.codePromoId = promo;
    order.foods = _foods;

    AuthStorage.getCurrentUser().then((user) {
      order.memberId = user.id;

      OrderService.createOrder(order).then((success) {
        String msg = success
            ? 'Commande enregistré.'
            : 'Erreur : commande non enregistré';

        if (success) {
          CartStorage.deleteCart().then((value) {
            super.widget.onDeleteItem(0);
            setState(() {
              _foods.removeRange(0, _foods.length);
              _codePromo = '';
              _totalFoods = null;
              _totalOrder = null;
              futureValue = CartStorage.getCart();
            });
          });
        }

        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(msg)));
      });
    });
  }
}
