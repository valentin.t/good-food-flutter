import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

import '../../../storage/auth_storage.dart';

import '../../../services/order_service.dart';

import '../../../models/member.dart';
import '../../../models/food.dart';
import '../../../models/order.dart';

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  State<History> createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  Member? _user;
  List<Order> _orders = List<Order>.empty();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getData(),
      builder: ((context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting ||
            !snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else {
          return _getScreen();
        }
      }),
    );
  }

  Future<bool> _getData() async {
    _user = await AuthStorage.getCurrentUser();
    _orders = await OrderService.getOrders(_user?.id ?? 0);

    return true;
  }

  Widget _getScreen() {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text('Mes commandes'),
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 15, left: 10, right: 10),
        child: _getHistory(),
      ),
    );
  }

  Widget _getHistory() {
    if (_orders.isEmpty) {
      return const Center(child: Text('Aucun historique !'));
    }

    return ListView.separated(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: _orders.length,
      separatorBuilder: (context, index) => const Divider(),
      itemBuilder: (context, index) {
        final currentOrder = _orders[index];

        Widget orderAmount =
            currentOrder.displayDiscount != currentOrder.displayTotal
                ? FittedBox(
                    fit: BoxFit.fill,
                    child: Column(
                      children: [
                        Text(currentOrder.displayTotal,
                            style: const TextStyle(
                                decoration: TextDecoration.lineThrough)),
                        //const SizedBox(width: 10),
                        Text(currentOrder.displayDiscount)
                      ],
                    ))
                : Text(currentOrder.displayDiscount);

        return ExpansionTile(
            backgroundColor: currentOrder.historyBackgroundColor(context),
            collapsedBackgroundColor: currentOrder.historyBackgroundColor(context),
            title: Text('Commande n°${currentOrder.id}'),
            subtitle:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(currentOrder.displayDate),
                  Text(currentOrder.orderStatus?.name ?? 'Aucun statut')
            ]),
            trailing: orderAmount,
            controlAffinity: ListTileControlAffinity.leading,
            children: _generateDetails(currentOrder.foods));
      },
    );
  }

  List<Widget> _generateDetails(List<Food> foods) {
    var tiles = List<ListTile>.empty(growable: true);
    var grouped = groupBy(foods, (Food f) => f.id);

    for (var entry in grouped.entries) {
      tiles.add(ListTile(
        title: Row(children: [
          Text(entry.value.first.name),
          const Spacer(),
          Text('(x${entry.value.length})')
        ]),
        subtitle: Row(children: [
          const Spacer(),
          Text('${entry.value.map((e) => e.price).sum.toStringAsFixed(2)} €')
        ]),
      ));
    }

    return tiles;
  }
}
