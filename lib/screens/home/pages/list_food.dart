import 'package:flutter/material.dart';

import '../../../theme/style.dart';

import '../../../services/food_service.dart';

import '../../../storage/auth_storage.dart';
import '../../../storage/cart_storage.dart';

import '../../../models/category.dart';
import '../../../models/food.dart';
import '../../../models/member.dart';

class ListFood extends StatefulWidget {
  final ValueChanged<int> onAddItem;
  const ListFood({Key? key, required this.onAddItem}) : super(key: key);

  @override
  State<ListFood> createState() => _ListFoodState();
}

class _ListFoodState extends State<ListFood> {
  Future<List<Food>>? _futureValue;
  late List<Category> _categories;
  late Member _member;
  late List<Food> _foods;
  late int _selectedValue;

  @override
  void initState() {
    FoodService.getCategories().then((value) {
      _categories = value;
      if (_categories.isNotEmpty) {
        _onChangeCategory(_categories.first.id);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_futureValue == null) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }

    return FutureBuilder<List<dynamic>>(
      future: Future.wait([AuthStorage.getCurrentUser(), _futureValue!]),
      builder: ((context, snapshot) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else {
          _member = snapshot.data?[0] as Member;
          _foods = (snapshot.data?[1] as List<Food>?)
                  ?.where((food) => food.isActive)
                  .toList() ??
              List<Food>.empty();
          return getScreen();
        }
      }),
    );
  }

  Scaffold getScreen() {
    final listItems = _getItems();
    final Widget list = _getListFoods();

    return Scaffold(
      appBar: AppBar(
          leading: const Icon(Icons.location_on_outlined),
          title: Text(_member.adress?.toString() ?? 'Aucune adresse')),
      body: Container(
        margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
        child: Column(
          children: <Widget>[
            DropdownButtonFormField<int>(
              value: _selectedValue,
              icon: const Icon(Icons.arrow_drop_down_circle_outlined),
              elevation: 16,
              decoration: const InputDecoration(
                  border: UnderlineInputBorder(), labelText: 'Nos catégories'),
              onChanged: _onChangeCategory,
              dropdownColor: theme.isDarkTheme ? null : Colors.white,
              items: listItems,
            ),
            const SizedBox(height: 20),
            Expanded(child: list)
          ],
        ),
      ),
    );
  }

  List<DropdownMenuItem<int>> _getItems() {
    return _categories.map<DropdownMenuItem<int>>((value) {
      return DropdownMenuItem<int>(
        value: value.id,
        child: Text(value.name),
      );
    }).toList();
  }

  Widget _getListFoods() {
    if (_foods.isEmpty) {
      return const Center(
          child: Text('Aucun plat correspondant à cette catégorie'));
    }

    return ListView.separated(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      itemCount: _foods.length,
      separatorBuilder: (context, index) => const Divider(),
      itemBuilder: (context, index) {
        final currentFood = _foods[index];
        Widget img = currentFood.urlImage != null
            ? Image.network(
                currentFood.urlImage!,
                width: 50,
                height: 50,
                fit: BoxFit.fill,
              )
            : const SizedBox(
                width: 50,
                height: 50,
              );

        return ListTile(
          title: Text(currentFood.name),
          subtitle: Text(currentFood.displayPrice),
          leading: img,
          trailing: Visibility(
            visible: currentFood.averageReview != null,
            child: Row(mainAxisSize: MainAxisSize.min, children: [
              Text('${currentFood.averageReview?.toStringAsFixed(1)}'),
              Icon(
                Icons.star,
                color: theme.isDarkTheme ? null : Colors.black,
              )
            ]),
          ),
          onLongPress: () => _addToCart(currentFood),
          onTap: () => Navigator.pushNamed(context, '/detail',
                  arguments: <dynamic>[currentFood, super.widget.onAddItem])
              .whenComplete(() => setState(() => _foods[index])),
        );
      },
    );
  }

  void _addToCart(Food food) {
    CartStorage.createOrAddToCart(food).then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Plat ajouté'),
          duration: Duration(seconds: 2),
        ),
      );
    }).whenComplete(() {
      CartStorage.getCart()
          .then((value) => super.widget.onAddItem(value.length));
    });
  }

  void _onChangeCategory(int? newValue) {
    setState(() {
      _selectedValue = newValue!;
      _futureValue = FoodService.getFoodsByCategory(
          _categories.firstWhere((cat) => cat.id == _selectedValue));
    });
  }
}
