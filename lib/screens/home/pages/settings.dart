import 'package:flutter/material.dart';
import 'package:phone_form_field/phone_form_field.dart';

import '../../../theme/style.dart';
import '../../../validator.dart';

import '../../../storage/auth_storage.dart';

import '../../../services/auth_service.dart';

import '../../../models/adress.dart';
import '../../../models/member.dart';

class Settings extends StatefulWidget {
  // Pour forcer la navigationBar à changer de thème
  final VoidCallback onChangeTheme;
  const Settings({Key? key, required this.onChangeTheme}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final _formKey = GlobalKey<FormState>();
  Future<Member> futureValue = AuthStorage.getCurrentUser();
  late Member _member;
  late Member _initialMember;
  String _streetNumber = '';
  String _streetName = '';
  String _city = '';
  String _postcode = '';
  String _country = '';

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            automaticallyImplyLeading: false,
            title: const Text('Paramètres'),
            actions: [
              Icon(theme.isDarkTheme
                  ? Icons.nightlight_outlined
                  : Icons.light_mode_outlined),
              Switch(
                  value: theme.isDarkTheme,
                  onChanged: (value) {
                    setState(() => theme.switchTheme());
                    super.widget.onChangeTheme();
                  }),
              TextButton.icon(
                label: const Text('Déconnexion',
                    style: TextStyle(color: Colors.redAccent)),
                icon: const Icon(Icons.logout, color: Colors.redAccent),
                onPressed: _logout,
              )
            ],
          ),
          body: FutureBuilder<Member>(
            future: futureValue,
            builder: ((context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (snapshot.hasError) {
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              } else {
                _member = snapshot.data!;
                _initialMember = Member.clone(_member);

                _streetNumber = _member.adress?.streetNumber.toString() ?? '';
                _streetName = _member.adress?.street ?? '';
                _city = _member.adress?.city ?? '';
                _postcode = _member.adress?.postcode ?? '';
                _country = _member.adress?.country ?? '';

                return _getForm();
              }
            }),
          ),
        ));
  }

  Container _getForm() {
    return Container(
      margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.fingerprint_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Prénom',
              ),
              initialValue: _member.firstname,
              keyboardType: TextInputType.name,
              maxLength: 50,
              onChanged: (value) => _member.firstname = value.trim(),
              validator: (value) => Validator.validate(value),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.person_outline),
                border: UnderlineInputBorder(),
                labelText: 'Nom',
              ),
              initialValue: _member.lastname,
              keyboardType: TextInputType.name,
              maxLength: 50,
              onChanged: (value) => _member.lastname = value.trim(),
              validator: (value) => Validator.validate(value),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.alternate_email),
                border: UnderlineInputBorder(),
                labelText: 'Email',
              ),
              keyboardType: TextInputType.emailAddress,
              maxLength: 50,
              initialValue: _member.email,
              onChanged: (value) => _member.email = value.trim(),
              validator: (value) => Validator.validateEmail(value),
            ),
            const SizedBox(height: 5),
            PhoneFormField(
              key: const Key('phone-field'),
              initialValue:
                  _member.phone != null && _member.phone.toString().isNotEmpty
                      ? PhoneNumber.parse(_member.phone!,
                          callerCountry: IsoCode.FR,
                          destinationCountry: IsoCode.FR)
                      : null,
              shouldFormat: true,
              defaultCountry: IsoCode.FR,
              decoration: const InputDecoration(
                labelText: 'Téléphone',
                prefixIcon: Icon(Icons.phone),
              ),
              isCountryChipPersistent: false,
              isCountrySelectionEnabled: false,
              countrySelectorNavigator:
                  const CountrySelectorNavigator.bottomSheet(),
              showFlagInInput: false,
              flagSize: 16,
              onChanged: (value) => _member.phone = '0${value!.nsn.trim()}',
              validator: PhoneValidator.validMobile(),
            ),
            const SizedBox(height: 15),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.map_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Numéro de rue',
              ),
              keyboardType: TextInputType.number,
              maxLength: 3,
              initialValue: _streetNumber,
              onChanged: (value) => _streetNumber = value.trim(),
              validator: (value) => Validator.validateInt(value),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.location_on_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Nom de rue',
              ),
              initialValue: _streetName,
              keyboardType: TextInputType.name,
              maxLength: 50,
              onChanged: (value) => _streetName = value.trim(),
              validator: (value) => Validator.validate(value),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.home_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Ville',
              ),
              initialValue: _city,
              keyboardType: TextInputType.name,
              maxLength: 50,
              onChanged: (value) => _city = value.trim(),
              validator: (value) => Validator.validate(value),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.flag_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Code postal',
              ),
              initialValue: _postcode,
              keyboardType: TextInputType.number,
              maxLength: 5,
              onChanged: (value) => _postcode = value.trim(),
              validator: (value) => Validator.validateInt(value, digits: 5),
            ),
            const SizedBox(height: 5),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.public_outlined),
                border: UnderlineInputBorder(),
                labelText: 'Pays',
              ),
              initialValue: _country,
              keyboardType: TextInputType.name,
              maxLength: 50,
              onChanged: (value) => _country = value.trim(),
              validator: (value) => Validator.validate(value),
            ),
            const SizedBox(height: 5),
            ElevatedButton(
              style: elevatedButtonStyle(),
              onPressed: _update,
              child: const Text('METTRE À JOUR'),
            ),
          ],
        ),
      ),
    );
  }

  void _logout() {
    AuthService.disconnectAccount().then((result) {
      if (!result) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Non déconnecté')),
        );
        return;
      }

      AuthStorage.deleteUser().then((value) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Déconnecté'),
          ),
        );

        pushNotificationService.unsubscribeTopic().whenComplete(() => Navigator.pushNamed(context, '/login'));
      });
    });
  }

  void _update() {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    if (_member.adress == null) {
      _member.adress = Adress(
          id: -1,
          street: _streetName,
          streetNumber: int.parse(_streetNumber),
          city: _city,
          postcode: _postcode,
          country: _country,
          createdAt: DateTime.now(),
          updatedAt: DateTime.now());
    } else {
      _member.adress?.street = _streetName;
      _member.adress?.streetNumber = int.parse(_streetNumber);
      _member.adress?.city = _city;
      _member.adress?.postcode = _postcode;
      _member.adress?.country = _country;
    }

    AuthService.updateMember(_member, _initialMember).then((result) {
      if (!result) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Erreur de mise à jour.')),
        );
        return;
      }

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Utilisateur mis à jour'),
        ),
      );

      setState(() {
        _streetNumber = '';
        _streetName = '';
        _city = '';
        _postcode = '';
        _country = '';
      });
    });
  }
}
