import 'dart:convert';
import 'package:http/http.dart' as http;

import './http_utils.dart';

import '../storage/auth_storage.dart';

import '../models/member.dart';

class AuthService {
  static Future<MapEntry<bool, String>> registerAccount(String firstname, String lastname, String email, String password) async {
    var postBody =  {
      'member': {
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'password': password,
        'rank_id': 4 // client
      }
    };

    var response = await http.post(HttpUtils.getUri('members'), headers: HttpUtils.headers, body: jsonEncode(postBody));

    return MapEntry(response.statusCode == 200, response.body);
  }

  static Future<MapEntry<bool, String>> connectAccount(String email, String password) async {
    var postBody =  {
      'member': {
        'email': email,
        'password': password
      }
    };

    var response = await http.post(HttpUtils.getUri('auth/login'), headers: HttpUtils.headers, body: jsonEncode(postBody));
    var data = jsonDecode(response.body);
    var isConnected = response.statusCode == 200;

    if (isConnected) {
      var member = Member.fromJson(data);
      AuthStorage.setCurrentUser(member);
    }

    return MapEntry(isConnected, (isConnected ? 'Utilisateur ${data['member']['email']} connecté' : data['error']) );
  }

  static Future<bool> disconnectAccount() async {
    var response = await http.get(HttpUtils.getUri('auth/logout'));

    return response.statusCode == 200;
  }

  static Future<bool> isLoggedIn() async {
    bool isUser = await AuthStorage.isUserSaved();

    if (!isUser) {
      return false;
    }

    Member user = await AuthStorage.getCurrentUser();
    var response = await http.get(HttpUtils.getUri('members/${user.id}'));
    var data = jsonDecode(response.body) as Map<String, dynamic>;
    var isLogged = data['data'] != null;

    if (isLogged) {
      // Update infos user
      var member = Member.fromUpdateJson(data['data']);
      AuthStorage.setCurrentUser(member);
    }

    return isLogged;
  }

  static Future<bool> updateMember(Member member, Member initialMember) async {
    var postBody =  {
      'member': member.toJson(initialMember)
    };

    var response = await http.put(HttpUtils.getUri('members/${member.id}'), headers: HttpUtils.headers, body: jsonEncode(postBody));
    var data = jsonDecode(response.body) as Map<String, dynamic>;
    var success = response.statusCode == 200;

    if (success && data.containsKey('data')) {
      // Update infos user
      var member = Member.fromUpdateJson(data['data'] as dynamic);
      AuthStorage.setCurrentUser(member);
    }

    return success;
  }
}