import 'dart:convert';
import 'package:good_food_flutter/models/review.dart';
import 'package:http/http.dart' as http;

import './http_utils.dart';

import '../models/food.dart';
import '../models/category.dart';
import '../models/review.dart';

class FoodService {
  static Future<List<Food>> getFoodsByCategory(Category category) async {
    var foods = List<Food>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('foods/category/${category.id}'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body) as List<dynamic>;
      foods = json.map((e) => Food.fromJson(e as Map<String, dynamic>)).toList();
    }

    return foods;
  }

  static Future<List<Category>> getCategories() async {
    var categories = List<Category>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('categories'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body) as Map<String, dynamic>;
      var data = json['data'] as List<dynamic>;

      categories = data.map((e) => Category.fromJson(e as Map<String, dynamic>)).toList();
    }

    return categories;
  }

  static Future<List<Review>> getReviewsOfFood(Food food) async {
    var reviews = List<Review>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('reviews/food/${food.id}'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      var data = json['data'] as List<dynamic>;
      reviews = data.map((r) => Review.fromJson(r as Map<String, dynamic>)).toList();
    }

    return reviews;
  }

  static Future<bool> createReview(Review review) async {
    var postBody =  {
      'review' : review.toJson()
    };

    var response = await http.post(HttpUtils.getUri('reviews'), headers: HttpUtils.headers, body: jsonEncode(postBody));

    return response.statusCode == 200;
  }
}