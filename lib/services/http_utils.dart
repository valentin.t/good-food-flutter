import '../environment/environment.dart';

class HttpUtils {
  static const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
  };

  static Uri getUri(String route) {
    return Uri(scheme: 'https', host: URL_BACK_END, path: 'api/$route');
  }
}