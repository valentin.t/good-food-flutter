import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';

import '../theme/style.dart';

class NotificationService {
  static const _key = 'token';
  String? _currentTopic;
  final FirebaseMessaging _fcm;

  NotificationService(this._fcm);

  Future initialise() async {
    /*if (Platform.isIOS) {
      FirebaseMessaging.requestNotificationPermissions(IosNotificationSettings());
    }*/

    String? token = await _fcm.getToken();
    if (token != null) {
      box!.put(_key, token);
    }
    _fcm.onTokenRefresh.listen((newToken) => box!.put(_key, newToken));

    FirebaseMessaging.onMessage.listen(_onMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpened);
    FirebaseMessaging.onBackgroundMessage(_onBackgroundMessage);
  }

  static void _onMessage(RemoteMessage message) {
    // Lors de la réception du message pendant que l'appli est ouverte
    // On peut par exemple mettre en place des notifications personnalisé dans l'appli
    // étant donné que rien est affiché
    print('titre : ${message.notification!.title} | contenu : ${message.notification!.body}');
  }

  static void _onMessageOpened(RemoteMessage message) {
    // Lorsque l'utilisateur clique sur la notif
    // On peut par exemple ouvrir l'appli à une page spécifique en fonction des données dans le message
  }

  static Future<void> _onBackgroundMessage(RemoteMessage message) async {
    // Lors de la réception du message pendant que l'appli en arrière plan ou bien stoppé
    // Une notification est bien affiché dans ce cas
    print('message en arrière plan : ${message.notification!.title}, ${message.notification!.body}');
  }

  Future<void> subscribeTopic(String topic) async {
    await _fcm.subscribeToTopic(topic);
    _currentTopic = topic;
  }

  Future<void> unsubscribeTopic() async {
    await _fcm.unsubscribeFromTopic(_currentTopic.toString());
  }
}