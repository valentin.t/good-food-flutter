import 'dart:convert';
import 'package:http/http.dart' as http;

import './http_utils.dart';

import '../models/order.dart';
import '../models/order_status.dart';

class OrderService {
  static Future<List<Order>> getOrdersByStatus(OrderStatus status) async {
    var orders = List<Order>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('orders/status/$status.id'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body) as List<Map<String, dynamic>>;
      json.map((e) => orders.add(Order.fromJson(e)));
    }

    return orders;
  }

  static Future<List<OrderStatus>> getOrderStatuses() async {
    var orderStatuses = List<OrderStatus>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('order_statuses'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body) as List<Map<String, dynamic>>;
      json.map((e) => orderStatuses.add(OrderStatus.fromJson(e)));
    }

    return orderStatuses;
  }

  // Récupère les commandes du client connecté
  static Future<List<Order>> getOrders(int userId) async {
    var orders = List<Order>.empty(growable: true);
    var response = await http.get(HttpUtils.getUri('orders/mobile/$userId'));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body) as List<dynamic>;

      orders = json.map((e) => Order.fromJson(e as Map<String, dynamic>)).toList();
    }

    return orders;
  }

  // Application code promo si possible
  // Retourne la valeur de remise et l'id du code promo. Les valeurs sont nulles si le code n'existe pas.
  static Future<MapEntry<double?, int?>> getDiscount(String discountCode, double totalAmount) async {
    var postBody =  {
      'total_amount': totalAmount
    };

    var response = await http.post(HttpUtils.getUri('code_promos/apply/$discountCode'), headers: HttpUtils.headers, body: jsonEncode(postBody));

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return MapEntry(double.parse(json['remise']), json['code_promo_id'] as int);
    }

    return const MapEntry(null, null);
  }

  static Future<bool> createOrder(Order order) async {
    var postBody =  {
      'order' : order.toJson()
    };

    var response = await http.post(HttpUtils.getUri('orders'), headers: HttpUtils.headers, body: jsonEncode(postBody));

    return response.statusCode == 200;
  }

}