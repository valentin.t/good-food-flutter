import 'dart:convert';

import './storage_utils.dart';

import '../models/member.dart';

class AuthStorage {
  static const _key = 'auth';

  static Future<Member> getCurrentUser() async {
    var jsonMember = await StorageUtils.readStr(_key);

    var member = Member.fromStorage(jsonDecode(jsonMember.toString()));

    return member;
  }

  static setCurrentUser(Member member) async {
    var jsonMember = jsonEncode(member.toStorage());

    await StorageUtils.saveStr(_key, jsonMember);
  }
  
  static deleteUser() async {
    await StorageUtils.removeKey(_key);
  }

  static Future<bool> isUserSaved() async {
    var isSaved = await StorageUtils.keyExists(_key);

    return isSaved;
  }
}