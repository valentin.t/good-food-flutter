import 'dart:convert';

import './storage_utils.dart';

import '../models/food.dart';

class CartStorage {
  static const _key = 'cart';

  static Future<void> createOrAddToCart(Food food) async {
    var cartExists = await StorageUtils.keyExists(_key);
    List<Food> cart = List<Food>.empty(growable: true);

    if (cartExists) {
      cart = await getCart();
    }

    cart.add(food);

    await _setCart(cart);
  }

  static Future<void> deleteCartItem(int index) async {
    var cart = await getCart();

    cart.removeAt(index);

    await _setCart(cart);
  }

  static Future<void> deleteCart() async {
    await StorageUtils.removeKey(_key);
  }

  static Future<List<Food>> getCart() async {
    var storage = await StorageUtils.readStr(_key);

    if (storage != null && storage.isNotEmpty) {
      var data = jsonDecode(storage) as List<dynamic>;

      var cart = data.map((food) => Food.fromJson(food as Map<String, dynamic>)).toList(growable: true);

      return cart;
    }
    else {
      return List<Food>.empty();
    }
  }

  static Future<void> _setCart(List<Food> cart) async {
    await StorageUtils.saveStr(_key, jsonEncode(cart.map((e) => e.toJson()).toList()));
  }
}