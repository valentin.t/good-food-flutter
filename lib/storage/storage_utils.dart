import 'package:shared_preferences/shared_preferences.dart';

class StorageUtils {

  static Future<void> saveStr(String key, String message) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    var isSet = await pref.setString(key, message);
  }

  static Future<String?> readStr(String key) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(key);
  }

  static Future<bool> keyExists(String key) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.containsKey(key);
  }

  static Future<bool> removeKey(String key) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    var isRemoved = await pref.remove(key);
    return isRemoved;
  }
}