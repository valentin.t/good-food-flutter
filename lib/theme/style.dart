import 'package:flutter/material.dart';
import 'package:good_food_flutter/services/notification_service.dart';
import 'package:hive_flutter/hive_flutter.dart';

import './theme_manager.dart';

ThemeManager theme = ThemeManager();
Box? box;
late NotificationService pushNotificationService;

ThemeData appTheme() {
  return ThemeData(
      primaryColor: Colors.white,
      hintColor: Colors.grey,
      //dividerColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      canvasColor: Colors.black,
      colorScheme:
          ColorScheme.fromSwatch().copyWith(secondary: Colors.lightBlueAccent),
      appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white, foregroundColor: Colors.black));
}

ButtonStyle elevatedButtonStyle() {
  return ButtonStyle(
    minimumSize: MaterialStateProperty.all(const Size.fromHeight(40)),
  );
}

TextStyle dismissibleTextStyle() {
  return const TextStyle(
      color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold);
}

TextStyle totalFoodTextStyle() {
  return const TextStyle(
      fontSize: 20, fontWeight: FontWeight.bold);
}

TextStyle totalOrderTextStyle() {
  return const TextStyle(
      color: Colors.black,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      decoration: TextDecoration.lineThrough);
}