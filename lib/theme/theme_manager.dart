import 'package:flutter/material.dart';

import './style.dart';

class ThemeManager with ChangeNotifier {
  static const String _key = 'theme';
  static bool _isDark = false;

  bool get isDarkTheme => _isDark;

  ThemeManager() {
    if (box!.containsKey(_key)) {
      _isDark = box!.get(_key);
    }
    else {
      box!.put(_key, _isDark);
    }
  }

  ThemeMode currentTheme() {
    return _isDark ? ThemeMode.dark : ThemeMode.light;
  }

  void switchTheme() {
    _isDark = !_isDark;
    box!.put(_key, _isDark);
    notifyListeners();
  }
}