import 'package:email_validator/email_validator.dart';

// Classe contenant des méthodes communes de validation de formulaire.
class Validator {
  static String? validateEmail(String? value) {
    String? message = validate(value);

    if (message == null && !EmailValidator.validate(value.toString())) {
      message = 'Adresse mail invalide';
    }

    return message;
  }

  static String? validateInt(String? value, {int? digits, int? minValue, int? maxValue}) {
    String? message = validate(value);

    if (message == null && int.tryParse(value!) == null) {
      message = 'Numéro invalide';
    }

    if (message == null && digits != null && value!.length != digits) {
      message = '$digits chiffres attendus';
    }

    if (message == null && (minValue != null || maxValue != null)) {
      var tempInt = int.parse(value!);

      if (minValue != null && tempInt < minValue) {
        message = 'Valeur minimale $minValue non respectée.';
      }

      if (maxValue != null && tempInt > maxValue) {
        message = 'Valeur maximale $maxValue dépassée.';
      }
    }

    return message;
  }

  static String? validate(String? value) {
    if (value == null || value.toString().trim().isEmpty) {
      return 'Veuillez remplir ce champ';
    }
    return null;
  }
}
